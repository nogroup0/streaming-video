import React from "react";
import { Field, reduxForm } from 'redux-form';


const StreamForm = (props) => {
    const renderError = ({ touched, error }) => {
        if ( touched && error ) {
            return (
                <div className="ui error message">
                    <div className="header">{error}</div>
                </div>
            );
        };
    };

    const renderInput = ({input, label, meta}) => {
        const className = `field ${meta.touched && meta.error ? 'error' : ''}`;

        return (
            <div className={className}>
                <label>{label}</label>
                <input {...input} autoComplete="off" />
                {renderError(meta)}
            </div>
        )
    }

    const onSubmit = (formValues) => {
        props.onSubmit(formValues);
    };

    return (
        <Form 
            initialValues={props.initialValues}
            onSubmit={onSubmit}
            validate = {(formValues) => {
                const errors = {};
            
                if (!formValues.title) {
                    errors.title = "Please, enter a Title"
                }
                if (!formValues.description) {
                    errors.description = "Please, enter a Description"
                }
                return errors;
            }}
            render={({ handleSubmit }) => (
                <form onSubmit={handleSubmit} className="ui form error">
                    <Field name="title" component={renderInput} label="Enter Title"/>
                    <Field name="description" component={renderInput} label="Enter Description" />
                    <button className="ui button primary">Submit</button>
                </form>
            )}
        />  
    );
};




export default reduxForm({
    form: 'streamForm',
    validate: validate,
})(StreamForm);

