import React from "react";
import { connect } from 'react-redux';
import { signIn, signOut} from '../actions';

class GoogleAuth extends React.Component {
    state = { isSignedIn: null };

    componentDidMount() {
        window.gapi.load('client:auth2', () => {
            window.gapi.client.init({
                clientId:'852497136286-olbntc71frsum4t11e57mqjo4eda0bsf.apps.googleusercontent.com',
                scope:'email',
                plugin_name:'streamer'
            }).then(() => {
                this.auth = window.gapi.auth2.getAuthInstance();
                this.onAuthChange(this.auth.isSignedIn.get());
                this.auth.isSignedIn.listen(this.onAuthChange)
            });
        });
    };

    onAuthChange = isSignedIn => {
        if (isSignedIn) {
            this.props.signIn(this.auth.currentUser.get().getId());
        } else {
            this.props.signOut();
        }
    };

    onClickSignIn = () => {
        this.auth.signIn()
    };

    onClickSignOut = () => {
        this.auth.signOut()
    };


    renderAuthButton = () => {
        if (this.props.isSignedIn === null) {
            return null;
        } else if (this.props.isSignedIn) {
            return (
                <button className="ui red google button" onClick={this.onClickSignOut}>
                    <i className="google icon" />
                    Sign Out
                </button>
            )
        } else return (
            <button className="ui green google button" onClick={this.onClickSignIn}>
                <i className="google icon" />
                Sign In
            </button>
        );
    };

    render() {
        return (
            <div>{this.renderAuthButton()}</div>
        );
    };
};

const mapStateToProps = state => {
    return { isSignedIn: state.auth.isSignedIn };
};

export default connect(mapStateToProps, { signIn, signOut })(GoogleAuth);